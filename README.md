GitLab Toggle Integration
==============================

A short description of the project.

Project Organization
------------


    ├── notebooks                   <- Exaples to work before we have the api implemented
    ├── src                         <- app source
    │         └── gateway           <- Gateways to the diferent data sources we need
    │         └── services          <- Services that are ussed in the app
    │         └── tests             <- Test for all the things in the app
    │         └── __init__.py
    ├── .env.tempalte               <- Template of the environment variables
    ├── .gitignore                  <- Files to ingoner in the repository upload
    ├── .gitlab-ci.yml              <- CI process
    ├── docker-compose.yaml         <- Local compose to run the app
    ├── Dockerfile                  <- File with the compositon of the app
    ├── Dockerfile.image            <- File with the compositon of the app to ulpload to docker hub
    ├── README.md                   <- Description of the proyect
    └── requirements.txt            <- Requirements of the proyect

--------

## Environment instalation
For the project to run you need to install [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/)  
If you are in linux you may need to make this [configurations](https://docs.docker.com/engine/install/linux-postinstall/)

```shell
$ docker-compose up --build
```

## Environment variables
Environment variable | Example value | Required | Default
--- | --- | --- | ---
PYTHONPATH  | ${PYTHONPATH}:src | YES | ${PYTHONPATH}:src
TOGGL_API_KEY  | {TOGGL_API_KEY} | YES | 
TOGGL_WORKSPACE_ID  | {TOGGL_WORKSPACE_ID} | YES | 
GITLAB_PRIVATE_TOKEN  | {GITLAB_PRIVATE_TOKEN} | YES | 
GITLAB_SERVER_URL  | {GITLAB_SERVER_URL} | YES | 


### CI CD / Docker Hub
If you make some changes in the `DockerFile.image` build the image and push to the hub.
Also if you make changes on the requirements.txt
In the DockerFile.image you will see that we dont copy the files to the docker image, maintain it that way
```bash
$ docker build -f Dockerfile.image -t '{HUB_NAME_SPACE}/gitlab_toggle_integration'.
$ docker login
$ docker push {HUB_NAME_SPACE}/gitlab_toggle_integration
```