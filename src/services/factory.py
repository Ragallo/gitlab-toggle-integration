from src.gateways.gitlab import GitLabGateway
from src.gateways.toggl import TogglGateway
from src.services.gitlab_toggl_integration import GitLabTogglIntegrationService, GitLabTogglIntegrationParser


class ServicesFactory:

    def build_gitlab_toggl_integration_service(self):
        return GitLabTogglIntegrationService(
            gitlab_gateway=GitLabGateway(),
            toggl_gateway=TogglGateway(),
            parser=GitLabTogglIntegrationParser()
        )
