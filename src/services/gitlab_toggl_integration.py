from dataclasses import dataclass
from datetime import datetime
from typing import Dict, List
from xml.etree.ElementTree import ParseError

from src.gateways.gitlab import GitLabGateway
from src.gateways.toggl import TogglEntry, TogglGateway


@dataclass
class GitLabEntry:
    project_id: str
    issue_id: str
    duration: str


class GitLabTogglIntegrationParser:
    def __init__(self,
                 project_id_identifier: str = 'pid',
                 issue_id_identifier: str = 'iid',
                 project_and_issue_id_delimiter: str = ':',
                 entry_delimiter: str = '|'):
        """This class allows you to parse the strings coming from toggl and
        transform the hours in gitlab hours.
        For this task we need delimiters to parse the values, we use 4 with default
        values that you can change:

        Params:
            - project_id_identifier: str = 'pid'
            - issue_id_identifier: str = 'iid'
            - project_and_issue_id_delimiter: str = ':'
            - entry_delimiter: str = '|'

        This is an example of how a string wuld come:
            'pid:1 | iid:2 | title of the task'
        """
        self._project_id_identifier = project_id_identifier
        self._issue_id_identifier = issue_id_identifier
        self._project_and_issue_id_delimiter = project_and_issue_id_delimiter
        self._entry_delimiter = entry_delimiter

    def get_gitlab_entry(self, toggl_entry: TogglEntry) -> GitLabEntry:
        pid_and_iid = self._parse_project_id_and_issue_id(entry_description=toggl_entry.description)
        duration = self._get_human_hours_and_minutes(start_time=toggl_entry.start_time, end_time=toggl_entry.end_time)
        return GitLabEntry(
            project_id=pid_and_iid[self._project_id_identifier],
            issue_id=pid_and_iid[self._issue_id_identifier],
            duration=duration,
        )

    def _parse_project_id_and_issue_id(self, entry_description: str) -> Dict[str, str]:
        """Get the project_id and issue_id from a entry_description with the
        following format:
           pid:1 | iid:2 | title of the task
        """
        split_entry = entry_description.split(self._entry_delimiter)

        values = {
            f'{self._project_id_identifier}': '',
            f'{self._issue_id_identifier}': ''
        }

        for string in split_entry:

            if self._project_id_identifier in string:
                values[self._project_id_identifier] = string.split(self._project_and_issue_id_delimiter)[1].strip()

            if self._issue_id_identifier in string:
                values[self._issue_id_identifier] = string.split(self._project_and_issue_id_delimiter)[1].strip()

        if not values[self._project_id_identifier] or not values[self._issue_id_identifier]:
            raise ParseError(
                f'Not {self._project_id_identifier} or {self._issue_id_identifier} find on: {entry_description}'
            )

        return values

    def _get_human_hours_and_minutes(self, start_time: str, end_time: str) -> str:
        """Get the start time and the end time to calculate the duration.
        We dont use the duration field because it says its in seconds but
        doing the transformation we get strange values.
        """
        start_time = datetime.fromisoformat(start_time)
        end_time = datetime.fromisoformat(end_time)
        str_hours_rest = str(end_time - start_time)
        duration = datetime.strptime(str_hours_rest, '%H:%M:%S')
        return duration.strftime('%Hh%Mm')


class GitLabTogglIntegrationService:
    def __init__(self,
                 gitlab_gateway: GitLabGateway,
                 toggl_gateway: TogglGateway,
                 parser: GitLabTogglIntegrationParser):
        self._gitlab_gateway = gitlab_gateway
        self._toggl_gateway = toggl_gateway
        self._parser = parser

    def load_toggle_hours_into_gitlab_by_date(self, start_date: str, end_date: str):
        """Get the hours for the date from toggle, parse and transform the
        values to gitlab entries and save it in each issue.

        Params:
            - start_date: str (%Y-%m-%d format)
            - end_date: str (%Y-%m-%d format)
        """
        toggl_entries = self._toggl_gateway.get_entry_hours_by_day_for_workspace(
            start_date=start_date, end_date=end_date
        )
        gitlab_entries = self._get_gitlab_entries(toggl_entries=toggl_entries)

        if not gitlab_entries:
            error_string = f'No entries for {start_date} to {end_date}, please, read the log to fin the cause.'
            print(error_string)
            return False, error_string

        self._save_gitlab_entries(gitlab_entries=gitlab_entries)
        saved_string = f'Values saved for {start_date} to {end_date}'
        return True, saved_string

    def reset_time_spent_for_issues_by_date(self, start_date: str, end_date: str):
        """Reset the time spent based on the toggle issues.
        This is because you think you make doble saves, but reset all the time spent,
        so be careful

        Params:
            - start_date: str (%Y-%m-%d format)
            - end_date: str (%Y-%m-%d format)
        """
        toggl_entries = self._toggl_gateway.get_entry_hours_by_day_for_workspace(
            start_date=start_date, end_date=end_date
        )
        gitlab_entries = self._get_gitlab_entries(toggl_entries=toggl_entries)

        if not gitlab_entries:
            error_string = f'No entries for {start_date} to {end_date}, please, read the log to fin the cause.'
            print(error_string)
            return False, error_string

        self._reset_time_spent_on_gitlab_entries(gitlab_entries=gitlab_entries)
        reset_string = f'Values reset for {start_date} to {end_date}'
        return True, reset_string

    def _get_gitlab_entries(self, toggl_entries: List[TogglEntry]):
        gitlab_entries = []

        for entry in toggl_entries:
            try:
                gitlab_entries.append(self._parser.get_gitlab_entry(toggl_entry=entry))
            except ParseError as parse_error:
                print(parse_error)

        return gitlab_entries

    def _save_gitlab_entries(self, gitlab_entries: List[GitLabEntry]):
        for entry in gitlab_entries:
            try:
                self._gitlab_gateway.add_spent_time_to_issue(
                    project_id=entry.project_id, issue_iid=entry.issue_id, time_spent=entry.duration
                )
                print(f'Saved {entry.duration} time for project {entry.project_id} - issue {entry.issue_id}')
            except Exception as exc:
                print(exc)

    def _reset_time_spent_on_gitlab_entries(self, gitlab_entries: List[GitLabEntry]):
        for entry in gitlab_entries:
            try:
                self._gitlab_gateway.reset_time_spend_to_issue(
                    project_id=entry.project_id, issue_iid=entry.issue_id,
                )
                print(f'Deleted time for project {entry.project_id} - issue {entry.issue_id}')
            except Exception as exc:
                print(exc)
