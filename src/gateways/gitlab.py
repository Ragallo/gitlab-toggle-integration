from enum import Enum
from os import environ

from gitlab import Gitlab
from gitlab.v4.objects import Project


class GitLabProjectsEnum(Enum):
    gitlab_toggl_integration = 25110200
    logo_study_assistant = 23118827


class GitLabGateway:
    _PRIVATE_TOKEN = environ.get('GITLAB_PRIVATE_TOKEN')
    _SERVER_URL = environ.get('GITLAB_SERVER_URL')
    _connected = False

    def get_projects_ids(self):
        """Get all the projects where the user is member"""
        self._connect()
        projects = self._client.projects.list(all=True, membership=True)

        if not projects:
            return []

        projects_values = []

        for project in projects:
            projects_values.append(dict(id=project.id, name=project.name))

        return projects_values

    def get_project_issues(self, project_id: str):
        """Get all the issues for one project"""
        project = self._get_project_by_id(project_id=project_id)
        issues = project.issues.list(all=True)

        issues_values = []
        for issue in issues:
            issues_values.append(dict(
                id=issue.id,
                iid=issue.iid,
                title=issue.title
            ))

        return issues_values

    def add_spent_time_to_issue(self, project_id: str, issue_iid: str, time_spent: str):
        """Insert time spend on a project issue
        https://docs.gitlab.com/ee/api/issues.html#add-spent-time-for-an-issue

        params:
            - project_id: int
            - issue_iid: int
            - time_spent: str (3h20m format)
        """
        project = self._get_project_by_id(project_id=project_id)
        issue = project.issues.get(id=issue_iid)
        return issue.add_spent_time(time_spent)

    def reset_time_spend_to_issue(self, project_id: str, issue_iid: str):
        """Insert time spend on a project issue
        https://docs.gitlab.com/ee/api/issues.html#reset-spent-time-for-an-issue

        params:
            - project_id: int
            - issue_iid: int
            - time_spent: str (3h20m format)
        """
        project = self._get_project_by_id(project_id=project_id)
        issue = project.issues.get(id=issue_iid)
        return issue.reset_spent_time()

    def _get_project_by_id(self, project_id: str) -> Project:
        self._connect()
        return self._client.projects.get(project_id)

    def _connect(self):
        if not self._connected:
            self._client: Gitlab = Gitlab(url=self._SERVER_URL, private_token=self._PRIVATE_TOKEN)
            self._client.auth()
            self._connected = True
