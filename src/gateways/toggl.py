from dataclasses import dataclass
from os import environ
from typing import List

from toggl.TogglPy import Toggl


@dataclass
class TogglEntry:
    description: str
    start_time: str
    end_time: str
    duration: int


class TogglGateway:
    _API_KEY = environ.get('TOGGL_API_KEY')
    _WORKSPACE_ID = environ.get('TOGGL_WORKSPACE_ID')
    _connected = False

    def get_entry_hours_by_day_for_workspace(
            self, start_date: str, end_date: str, workspace_id: str = None) -> List[TogglEntry]:
        """Get the fundamental values for the entries by hour

        Params:
            - start_date: str (%Y-%m-%d format)
            - end_date: str (%Y-%m-%d format)
            - workspace_id: str
        """
        self._connect()
        if not workspace_id:
            workspace_id = self._WORKSPACE_ID

        data = dict(workspace_id=workspace_id, since=start_date, until=end_date)
        report = self._client.getDetailedReport(data)

        if not report:
            return []

        clean_report = []
        for entry in report['data']:
            clean_report.append(
                TogglEntry(
                    description=entry['description'],
                    start_time=entry['start'],
                    end_time=entry['end'],
                    duration=entry['dur'],
                )
            )

        return clean_report

    def get_workspaces_ids(self):
        self._connect()
        raw_work_spaces = self._client.getWorkspaces()

        if not raw_work_spaces:
            return []

        return [
            dict(id=work_space['id'], name=work_space['name'])
            for work_space in raw_work_spaces
        ]

    def _connect(self):
        if not self._connected:
            self._client: Toggl = Toggl()
            self._client.setAPIKey(self._API_KEY)
            self._connected = True


