from src.gateways.toggl import TogglEntry
from src.services.gitlab_toggl_integration import GitLabTogglIntegrationParser, GitLabEntry


def test_gitlab_toggl_integration_parse_project_id_and_issue_id():
    integration = GitLabTogglIntegrationParser()
    entry_description = 'pid:183 | iid:5 | Create GitlabToggleIntegrationService'
    ids = integration._parse_project_id_and_issue_id(entry_description=entry_description)

    assert ids[integration._project_id_identifier] == '183'
    assert ids[integration._issue_id_identifier] == '5'

    entry_description = 'pid:108 | iid:26  | Create test class'
    ids = integration._parse_project_id_and_issue_id(entry_description=entry_description)

    assert ids[integration._project_id_identifier] == '108'
    assert ids[integration._issue_id_identifier] == '26'

    entry_description = 'pid:183 | iid:1 | Investigar integraciones y librerías a utilizar, crear issues con estructura de trabajo'
    ids = integration._parse_project_id_and_issue_id(entry_description=entry_description)

    assert ids[integration._project_id_identifier] == '183'
    assert ids[integration._issue_id_identifier] == '1'


def test_gitlab_toggl_integration_parse_duration():
    integration = GitLabTogglIntegrationParser()
    toggl_entry = TogglEntry(
        description='pid:183 | iid:5 | Create GitlabToggleIntegrationService',
        start_time='2021-03-08T17:47:35-03:00',
        end_time='2021-03-08T18:00:35-03:00',
        duration=780000,
    )

    parse_duration = integration._get_human_hours_and_minutes(
        start_time=toggl_entry.start_time,
        end_time=toggl_entry.end_time,
    )
    expected_duration = '00h13m'

    assert parse_duration == expected_duration

    toggl_entry = TogglEntry(
        description='pid:183 | iid:4 |  GitlabGateway',
        start_time='2021-03-08T15:07:29-03:00',
        end_time='2021-03-08T17:47:26-03:00',
        duration=9597000,
    )
    parse_duration = integration._get_human_hours_and_minutes(
        start_time=toggl_entry.start_time,
        end_time=toggl_entry.end_time,
    )
    expected_duration = '02h39m'

    assert parse_duration == expected_duration

    toggl_entry = TogglEntry(
        description='pid:183 | iid:3 | ToggleGateway',
        start_time='2021-03-08T10:05:46-03:00',
        end_time='2021-03-08T12:00:03-03:00',
        duration=6857000,
    )

    parse_duration = integration._get_human_hours_and_minutes(
        start_time=toggl_entry.start_time,
        end_time=toggl_entry.end_time,
    )
    expected_duration = '01h54m'

    assert parse_duration == expected_duration
    

def test_gitlab_toggl_integration_get_gitlab_entry():
    integration = GitLabTogglIntegrationParser()
    toggl_entry = TogglEntry(
        description='pid:183 | iid:5 | Create GitlabToggleIntegrationService',
        start_time='2021-03-08T17:47:35-03:00',
        end_time='2021-03-08T18:00:35-03:00',
        duration=780000,
    )

    gitlab_entry = integration.get_gitlab_entry(toggl_entry=toggl_entry)
    expected_gitlab_entry = GitLabEntry(
        project_id='183',
        issue_id='5',
        duration='00h13m',
    )

    assert gitlab_entry == expected_gitlab_entry

    toggl_entry = TogglEntry(
        description='pid:183 | iid:4 |  GitlabGateway',
        start_time='2021-03-08T15:07:29-03:00',
        end_time='2021-03-08T17:47:26-03:00',
        duration=9597000,
    )
    gitlab_entry = integration.get_gitlab_entry(toggl_entry=toggl_entry)
    expected_gitlab_entry = GitLabEntry(
        project_id='183',
        issue_id='4',
        duration='02h39m',
    )

    assert gitlab_entry == expected_gitlab_entry

    toggl_entry = TogglEntry(
        description='pid:183 | iid:3 | ToggleGateway',
        start_time='2021-03-08T10:05:46-03:00',
        end_time='2021-03-08T12:00:03-03:00',
        duration=6857000,
    )

    gitlab_entry = integration.get_gitlab_entry(toggl_entry=toggl_entry)
    expected_gitlab_entry = GitLabEntry(
        project_id='183',
        issue_id='3',
        duration='01h54m',
    )

    assert gitlab_entry == expected_gitlab_entry
