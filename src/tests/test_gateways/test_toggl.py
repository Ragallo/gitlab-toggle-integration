import pytest

from src.gateways.toggl import TogglGateway


def test_toggle_get_workspaces():
    gateway = TogglGateway()
    work_spaces_ids = gateway.get_workspaces_ids()

    assert work_spaces_ids


def test_toggle_get_hours_by_day_for_workspace():
    gateway = TogglGateway()
    report = gateway.get_entry_hours_by_day_for_workspace(start_date='2021-03-05', end_date='2021-03-05')

    assert report


@pytest.mark.skip(reason='This test made to explore the use o the class')
def test_try():
    gateway = TogglGateway()
    report = gateway.get_entry_hours_by_day_for_workspace(start_date='2021-03-08', end_date='2021-03-08')
    import ipdb; ipdb.set_trace()
