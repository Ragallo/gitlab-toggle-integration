import pytest
from gitlab.v4.objects import Project

from src.gateways.gitlab import GitLabGateway, GitLabProjectsEnum


def test_gitlab_gateway_get_projects_ids():
    gateway = GitLabGateway()
    projects = gateway.get_projects_ids()

    assert projects[0]['id']
    assert projects[0]['name']


def test_gitlab_gateway_get_project_by_id():
    gateway = GitLabGateway()
    project = gateway._get_project_by_id(project_id='25110200')

    assert type(project) == Project


@pytest.mark.skip(reason='This test edit the time in an issue so we dont want to run it')
def test_gitlab_gateway_add_spent_time_to_issue():
    gateway = GitLabGateway()
    time_spend = gateway.add_spent_time_to_issue(project_id='25110200', issue_iid='1', time_spent='3h5m')

    assert time_spend


@pytest.mark.skip(reason='This test edit the time in an issue so we dont want to run it')
def test_gitlab_gateway_add_spent_time_to_issue():
    gateway = GitLabGateway()
    time_reseted = gateway.reset_time_spend_to_issue(project_id='25110200', issue_iid='1')

    assert time_reseted


@pytest.mark.skip(reason='This is a manual run')
def test_manual_run_reset_time_spend():
    enums = GitLabProjectsEnum
    project_ids = [
        enums.gitlab_toggl_integration.value,
        enums.logo_study_assistant.value,
    ]

    gateway = GitLabGateway()
    for project_id in project_ids:
        project_issues = gateway.get_project_issues(project_id=project_id)

        for issue in project_issues:
            reset_time = gateway.reset_time_spend_to_issue(project_id=project_id, issue_iid=issue['iid'])
            print(f'\n{issue["title"]}\n{reset_time}\n____________')
