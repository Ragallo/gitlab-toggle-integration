# Repositories and Gaeways
python-gitlab==2.6.0
requests==2.24.0
TogglPy==0.1.1

# Test
pytest==5.4.2
pytest-sugar==0.9.4
freezegun==1.0.0
ipdb==0.13.3